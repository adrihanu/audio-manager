# Audio manager

Program let you choose audio output(main_client).
Other outputs are muted whenever your main_client is playing.
When main_client is quiet the other outputs are playing.

Tags: `Bash`, `audio`

## Runing

```bash
bash main.sh
```