#Program let you choose audio output(main_client).
#Other outputs are muted whenever your main_client is playing.
#When main_client is quiet the other outputs are playing.

# load virtual module with null-sink
function load_virtual_module(){
virtual_name="Virtual"
available_virtual_modules=$(pactl list short modules | grep $virtual_name | grep -o '[0-9]*')
if [ "$(printf "$available_virtual_modules" | grep -c .)" -ne "0" ]; then
	unload_module $available_virtual_modules; fi
virtual_module_index=$(pactl load-module module-null-sink sink_name=$virtual_name)
}


# unload_module (args: module_index)
function unload_module(){
	pactl unload-module $1
}


#get device_PCM_value (args: device_index)
function get_device_PCM_value(){
	device_PCM_value=$(parec --raw --device=$1 --channels=1 2>/dev/null | od -N2 -td2 | head -n1 | cut -d' ' -f2- | sed -e  's/ //g')
	printf %q "$device_PCM_value" #%q outputs an item that can be used as shell input
}	


#is device muted (args: device_index)
function is_device_muted(){		
printf %q "$(parec --raw  --device=$1 --channels=1 2>/dev/null | od -N50 -td2 | grep -c "*" 2>/dev/null )"
}


# wait till device is muted (args: device_index)
function wait_till_muted(){
	while [ "$(is_device_muted $1)" -eq "0" ]; do
		if [ "$(is_client_available ${client_index[$main_client_number]})" -eq "0" ]; then 
			unload_module $virtual_module_index
			exit
		fi
		sleep 0.4
		if [ "$(is_device_muted $1)" -ne "0" ];then sleep 0.2
			if [ "$(is_device_muted $1)" -ne "0" ];then sleep 0.2;fi
		fi 
	done
}


# wait till device is unmuted (args: device_index)
function wait_till_unmuted(){
	while [ "$(is_device_muted $1)" -ne "0" ]; do
		if [ "$(is_client_available ${client_index[$main_client_number]})" -eq "0" ]; then
			unload_module $virtual_module_index	
			exit
		else
			sleep 0.4; fi
	 done	
}


# move main_client to sink (args: sink_name)
function move_main_client_to_sink(){
			pacmd move-sink-input ${client_sink_input_index[$main_client_number]} $1
}


# move not_main_clients to sink (args: sink_name)
function move_not_main_client_to_sink(){
	for i in "${client_number[@]}"; do
		if [ "$i" != "$main_client_number" ]; then
	 		pacmd move-sink-input ${client_sink_input_index[$i]} $1; fi
	done
}


# get available clients
function get_available_clients(){
	i=1 && clear_clients
	available_clients_indexes=$(pacmd list-sink-inputs | grep "client:" | sed -e  's/<.*//g' -e 's/client: //g')
	while read index && [ -n "$available_clients_indexes" ]; do 
		client_number[$i]="$i"
		client_index[$i]="$index"
		client_name[$i]=$(pacmd list-sink-inputs | grep "client: ${client_index[$i]}" | sed -e  's/.*<//' -e 's/>//g' )
		client_sink_input_index[$i]=$(pacmd list-sink-inputs | grep -B20 "client: ${client_index[$i]}" | grep index | grep -o '[0-9]*')
		let "i+=1"
	done <<< "$available_clients_indexes"
}


#print available clients
function print_available_clients(){
	for i in "${client_number[@]}"; do
			print_client $i
	done
}


# print client (args: client_number)
function print_client(){
			printf "$1.${client_name[$1]} (${client_index[$1]}) (${client_sink_input_index[$1]}) \n"
}


# is client available (args: client_index)
function is_client_available(){
	printf %q "$(pacmd list-sink-inputs | grep -c "client: $1")"
} 


#set main_client_number
function set_main_client_number(){
	#read -d '' -t 0.1 -n 10000
	while read -p "Choose main_client_number(r=refresh,q=quit): " main_client_number; do	
		case "$main_client_number" in
			r) clear; get_available_clients;print_available_clients;;
			q) unload_module $virtual_module_index; exit ;;
		 	*) for i in "${client_number[@]}"; do
					if [ "$i" == "$main_client_number" ]; then break 2
					else
						clear; printf "No such client.\n"; 								  	
						get_available_clients;print_available_clients;				
					fi;  
				done;;
		esac
	done
}


# clear clients
function clear_clients(){
	client_number=()
	client_index=()
	client_name=()
	client_sink_input_index=()		
}


function get_sinks_names(){
default_sink_name=$(pacmd list-sinks | grep -A1 "* index:" | grep "name:"| sed -e 's/name: <//g' -e 's/>//g')
virtual_sink_name=$(pacmd list-sinks | grep  $virtual_name | sed -e 's/name: <//g' -e 's/>//g')
#printf "default_sink_name = $default_sink_name\n"
#printf "virtual_sink_name = $virtual_sink_name\n"
}


function get_sinks_indexes(){
default_sink_index=$(pacmd list-sinks | grep -B1 $default_sink_name | grep "index" | grep -o '[0-9]*')
virtual_sink_index=$(pacmd list-sinks | grep -B1 $virtual_sink_name | grep "index" | grep -o '[0-9]*')
#printf "default_sink_index = $default_sink_index\n"
#printf "virtual_sink_index = $virtual_sink_index\n"
}


function get_devices_indexes(){
default_device_index=$(pactl list | grep -B2 $default_sink_name | grep "#" | grep -o '[0-9]*' | tail -1)
virtual_device_index=$(pactl list | grep -B2 $virtual_sink_name | grep "#" | grep -o '[0-9]*' | tail -1) 
#printf "default_device_index = $default_device_index\n"
#printf "virtual_device_index = $virtual_device_index\n"
}


function start(){
	clear
	load_virtual_module
	get_sinks_names
	get_sinks_indexes
	get_devices_indexes
	get_available_clients
	print_available_clients
	set_main_client_number
	clear
	print_client $main_client_number
	printf "Use CTRL+c to quit."
	while true; do
		get_available_clients

		move_main_client_to_sink $default_sink_name
		move_not_main_client_to_sink $virtual_sink_name
		wait_till_muted $default_device_index

		move_main_client_to_sink $virtual_sink_name
		move_not_main_client_to_sink $default_sink_name
		wait_till_unmuted $virtual_device_index
	done
	# unload virtual module
	unload_module $virtual_module_index
	
	}

start
# PCM values of devices
#default_device_PCM_value="$(get_device_PCM_value $default_device_index)"
#virtual_device_PCM_value="$(get_device_PCM_value $virtual_device_index)"

